\babel@toc {italian}{}
\babel@toc {italian}{}
~\hfill \textbf {Page}\par 
\thispagestyle {empty}
\contentsline {chapter}{\numberline {1}Introduzione}{5}{chapter.1}%
\contentsline {chapter}{\numberline {2}$Advenias \ Care^{\scriptsize \textregistered }$}{7}{chapter.2}%
\contentsline {section}{\numberline {2.1}Stato del prodotto attuale}{7}{section.2.1}%
\contentsline {section}{\numberline {2.2}Modulo aggiunto}{8}{section.2.2}%
\contentsline {subsection}{\numberline {2.2.1}Obiettivo}{8}{subsection.2.2.1}%
\contentsline {subsection}{\numberline {2.2.2}Specifiche}{9}{subsection.2.2.2}%
\contentsline {subsection}{\numberline {2.2.3}Analisi funzionale}{9}{subsection.2.2.3}%
\contentsline {subsubsection}{Diari di struttura}{9}{subsection.2.2.3}%
\contentsline {subsubsection}{Verbali di riunione}{10}{subsection.2.2.3}%
\contentsline {chapter}{\numberline {3}Il progetto}{13}{chapter.3}%
\contentsline {section}{\numberline {3.1}Progettazione}{13}{section.3.1}%
\contentsline {section}{\numberline {3.2} Tecnologie di riferimento}{14}{section.3.2}%
\contentsline {subsection}{\numberline {3.2.1}BDMS}{15}{subsection.3.2.1}%
\contentsline {subsection}{\numberline {3.2.2}Framework Ruby on Rails}{15}{subsection.3.2.2}%
\contentsline {subsection}{\numberline {3.2.3}Vue e Quasar Framework}{17}{subsection.3.2.3}%
\contentsline {subsubsection}{Come creare una versione mobile della mia applicazione?}{18}{lstnumber.-2.2}%
\contentsline {chapter}{\numberline {4}Implementazione}{23}{chapter.4}%
\contentsline {section}{\numberline {4.1}Struttura del database}{23}{section.4.1}%
\contentsline {section}{\numberline {4.2}Back-end}{24}{section.4.2}%
\contentsline {subsection}{\numberline {4.2.1}Configurazione del database}{25}{subsection.4.2.1}%
\contentsline {subsection}{\numberline {4.2.2}Creazione del database}{27}{subsection.4.2.2}%
\contentsline {subsection}{\numberline {4.2.3}Avvio del Web Server}{27}{subsection.4.2.3}%
\contentsline {subsection}{\numberline {4.2.4}Creazione delle risorse}{27}{subsection.4.2.4}%
\contentsline {subsubsection}{Eseguire una migrazione}{28}{figure.4.3}%
\contentsline {subsection}{\numberline {4.2.5}Configurazione dei Modelli e dei Controllori}{29}{subsection.4.2.5}%
\contentsline {subsubsection}{Active storage}{29}{subsection.4.2.5}%
\contentsline {paragraph}{Allegare file ai modelli}{30}{lstnumber.-19.8}%
\contentsline {subsubsection}{Scops}{31}{lstnumber.-20.5}%
\contentsline {subsubsection}{Active Record Nested Attributes}{31}{lstnumber.-22.10}%
\contentsline {subsubsection}{Active Model Serialization}{33}{lstnumber.-24.12}%
\contentsline {section}{\numberline {4.3}Front-end}{34}{section.4.3}%
\contentsline {subsection}{\numberline {4.3.1}Configurazioni}{35}{subsection.4.3.1}%
\contentsline {subsubsection}{Plugin}{35}{subsection.4.3.1}%
\contentsline {subsubsection}{Dipendenze}{36}{subsection.4.3.1}%
\contentsline {subsection}{\numberline {4.3.2}Login}{37}{subsection.4.3.2}%
\contentsline {subsection}{\numberline {4.3.3}Home}{38}{subsection.4.3.3}%
\contentsline {subsection}{\numberline {4.3.4}Diario}{39}{subsection.4.3.4}%
\contentsline {subsection}{\numberline {4.3.5}Riunioni}{41}{subsection.4.3.5}%
\contentsline {chapter}{\numberline {5}Conclusioni e lavori futuri}{51}{chapter.5}%
\contentsline {section}{\numberline {5.1}Sviluppi successivi del progetto}{52}{section.5.1}%
\contentsline {chapter}{Bibliografia}{53}{chapter*.2}%
